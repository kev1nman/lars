<?php require_once("master.php"); cabecera(); ?>
<head>
  <!--Start of Zendesk Chat Script-->
			<script type="text/javascript">
			window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
			d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
			$.src="//v2.zopim.com/?43bVlFst2la5AkPi1ywevcXcic3UTyPR";z.t=+new Date;$.
			type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
			</script>
			<!--End of Zendesk Chat Script-->
</head>
<section class="sect2">
    <div class="corte post">
        <ul class="rslides" id="serviciosSlider">
            <li>
                <div class="slide1">
                    <div class="textSlide">
                        <h2>
                        ¡EN 10 DÍAS!
                    </h2>
                        <h3>
                        Creamos tu portal web fácil, rápido y adaptada a tu necesidad.
                    </h3>
                        <div class="precio">
                            <h2 class="caviar">
                                Desde 430.000 COP (+IVA)
                            </h2>
                        </div>
                        <a href="sitioWeb.php#sitioWeb" class="btn btn-primary btn-lg botonWhite">¡ VER MAS !</a>

                    </div>
                </div>
            </li>
            <li>
                <div class="slide2">
                    <div class="textSlide">
                        <h2>
                            ¿Listo para vender en internet?
                        </h2>
                        <h3>
                            En solo 10 días puedes emprender tu negocio en internet, estamos listos para cualquier reto, entendemos a la perfección las exigencias del mercado actual, TUS CLIENTES AGUARDAN POR TI!
                        </h3>
                        <div class="precio">
                            <h2 class="caviar">
                                Desde 800.000 COP (+IVA)
                            </h2>
                        </div>
                        <a href="sitioWeb.php#ecommerce" class="btn btn-primary btn-lg botonWhite">¡ QUIERO EMPEZAR A VENDER POR INTERNET !</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="slide3">
                    <div class="textSlide">
                        <h2>
                        ¿Cómo vender en Facebook?
                    </h2>
                        <h3>
                        Vender a través de Facebook ha revolucionado el mercado, Nuestros especialistas en Community Manager te mostrarán cómo.
                    </h3>
                        <div class="precio">
                            <h2 class="caviar">
                                Desde 430.000 COP (+IVA)
                            </h2>
                        </div>
                        <a href="communityManager.php" class="btn btn-primary btn-lg botonWhite">¡ VER MAS !</a>

                    </div>
                </div>
            </li>
            <li>
                <div class="slide4">
                    <div class="textSlide">
                        <h2>
                        ¡DISEÑAMOS TU IMAGEN!
                    </h2>
                        <h3>
                        Diseño gráfico a todo nivel y a tu alcance, nos adaptamos a tus ideas, le damos forma y color a tu visión.
                    </h3>
                        <div class="precio">
                            <h2 class="caviar">
                                Desde 430.000 COP (+IVA)
                            </h2>
                        </div>
                        <a href="disenoGrafico.php" class="btn btn-primary btn-lg botonWhite">¡ VER MAS !</a>

                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>


<section class="sect3">
    <div class="container post">
        <div class="row">
            <div class="col-xs-12 col-md-4 col-md-offset-2">
                <img src="img/phone.png" alt="">
            </div>
            <div class="ccol-xs-12 col-md-4 quienesSomos">
                <h1 class="">¿QUIENES SOMOS?</h1>
                <p>
                    Somos la opción ideal para tu desarrollo empresarial. Como tu aliado en tecnología, escuchamos y hacemos realidad tus ideas, somos un equipo de profesionales con más de 8 años de experiencia en el mercado digital y el desarrollo de aplicaciones web y móviles. Trabajamos CONTIGO, mano a mano para darle forma y color a tu proyecto, no solamente queremos prestarte un servicio, queremos ayudarte a alcanzar tus objetivos.
                </p>
                <img class="img-responsive center-block" src="img/iconsQS.png" alt="">
            </div>
        </div>
    </div>
</section>



<section class="sect4">
    <div class="container-fluid post">
        <div class="row">
            <div class="col-md-12">
                <h1>¿ Por qué Trabajar Con Nosotros ?</h1>
            </div>
            <div class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <img class="img-responsive center-block" src="img/ipad.png" alt="">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 porqNosotros">
                    <div class="caja">
                        <p>
                            Nuestro compromiso con tus proyectos es lo que nos define, no solo queremos prestarte un servicio, queremos ayudarte a alcanzar los objetivos de tu compañía.
                        </p>
                        <h3>TU ÉXITO, ES NUESTRO ÉXITO.</h3>
                    </div>
                    <div class="flecha"></div>
                    <a href="servicios.php" id='¿POR QUE TRABAJAR CON NOSOTROS?' class="click btn btn-primary btn-lg botonAzul">¡ COMENCEMOS YA !</a>
                </div>
            </div>
        </div>
    </div>
</section>




<section class="sect5">
    <div class="container post">
        <div class="row">
            <h1 id="Servicios">Servicios</h1>
            <div class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="icono">
                        <a href="disenoGrafico.php">
                            <img class="img-responsive icon1" src="img/servicios/1.png" alt="">
                            <img class="img-responsive icon11" src="img/servicios/11.png" alt="">
                        </a>
                    </div>
                    <div class="iconoTexto">
                        <a href="disenoGrafico.php">
                            <h4>Diseño Gráfico</h4>
                        </a>
                        <p>
                            -	Consultoría de Marca <br>
                            -	Diseño de Logotipo<br>
                            -	Diseño POP<br>
                            -	Papelería en General.

                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="linea desaparece768">
                        <div class="punta puntaL"></div>
                        <div class="punta puntaR"></div>
                    </div>
                    <div class="icono">
                        <a href="sitioWeb.php#sitioWeb">
                            <img class="img-responsive icon1" src="img/servicios/2.png" alt="">
                            <img class="img-responsive icon11" src="img/servicios/21.png" alt="">
                        </a>
                    </div>
                    <div class="iconoTexto">
                        <a href="sitioWeb.php#sitioWeb">
                            <h4>Páginas Web</h4>
                        </a>
                        <p>
                            Páginas web, en tiempo récord y a precios <b>increíbles!</b>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="linea desaparece">
                        <div class="punta puntaL"></div>
                        <div class="punta puntaR"></div>
                    </div>
                    <div class="icono">
                        <a href="sitioWeb.php#aplicacionesMoviles">
                            <img class="img-responsive icon1" src="img/servicios/3.png" alt="">
                            <img class="img-responsive icon11" src="img/servicios/31.png" alt="">
                        </a>
                    </div>
                    <div class="iconoTexto">
                        <a href="sitioWeb.php#aplicacionesMoviles">
                            <h4>Aplicaciones móviles</h4>
                        </a>
                        <p>
                            Desarrollo de aplicaciones móviles iOS® y Android®
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
                    <div class="linea aparece">
                        <div class="punta puntaL"></div>
                        <div class="punta puntaR"></div>
                    </div>
                    <div class="icono">
                        <a href="activityMonitor.php">
                            <img class="img-responsive icon1" src="img/servicios/4.png" alt="">
                            <img class="img-responsive icon11" src="img/servicios/41.png" alt="">
                        </a>
                    </div>
                    <div class="iconoTexto">
                        <a href="activityMonitor.php">
                            <h4>Monitorieo de Empleados</h4>
                        </a>
                        <p>
                            Visualización de escritorios de cualquier usuario de la red local en forma remota.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="linea desaparece">
                        <div class="punta puntaL"></div>
                        <div class="punta puntaR"></div>
                    </div>
                    <div class="icono">
                        <a href="communityManager.php">
                            <img class="img-responsive icon1" src="img/servicios/5.png" alt="">
                            <img class="img-responsive icon11" src="img/servicios/51.png" alt="">
                        </a>
                    </div>
                    <div class="iconoTexto">
                        <a href="communityManager.php">
                            <h4>Community Manager</h4>
                        </a>
                        <p>
                            Gestionamos tus redes con estrategia y creatividad.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="contact">
    <div class="container">
        <div class="row">
            <?php contact(); ?>
        </div>
    </div>
</section>





<?php footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#Home').addClass('active');
    });
</script>

</body>

</html>
