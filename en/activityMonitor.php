<?php require_once("master.php"); cabecera(); ?>

<div class="activityMonitor">

    <section class="sect4">
        <div class="container-fluid">
            <div class="row">
                <h1>Activity monitor ©</h1>
                <div class="col-xs-12 text-center">
                    <div class="textMonitor">
                        <h2>Do you really have control of what happens in your company?</h2>
                        <p>Well now if you can have it with Activity Monitor, with this software you can see in real time what your employees are doing at their workstations without them knowing it, you will know from which page they visited, how many hours they spent on Facebook, or even who filter <i>confidential information</i> of your company.
                        </p>
                        <a class="btn btn-info btnMonitor" href="#activityMonitor"><span class="glyphicon glyphicon-chevron-down"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="sect3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 id="activityMonitor" class="azul">Activity monitor Enterprise ®</h1>
                    <p>
                        Benefits such as viewing multiple computers on your network at the same time, generating reports, scheduling screen captures, recording keystrokes in applications, emails, web pages and chats are what this latest generation software offers you, <a href="http://cdn.softactivity.com/cdn/activmon.exe">¡Download now!</a>
                    </p>
                    <h2>The productivity of your Company is very important</h2>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <img class="img-responsive center-block img-thumbnail" src="img/remote.jpg" alt="">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 text-justify">
                    <h3>Online and Offline supervision of your employees</h3>
                    <p>
                        Trust, but check. Download Activity Monitor today and get the best employee monitoring solution for your company network.
                    </p>
                    <ul>
                        <li> Monitor your employees unknowingly </li>
                         <li> View visited web pages, used apps, instant messaging chats, email and more </li>
                         <li> Take screenshots and generate reports to take action </li>
                         <li> Easy to use </li>
                         <li> 1 year FREE Updates & Support </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <a href="javascript:showlightbox();" id='ACTIVITY MONITOR' class="click btn btn-primary btn-lg botonAzul">¡ FREE DOWNLOAD !</a>
                    <a href="javascript:showlightbox2();" id='ACTIVITY MONITOR COMPRAR' class="click btn btn-success btn-lg botonVerde">¡ BUY NOW !</a>
                </div>
            </div>
        </div>
    </section>





    <section class="contact">
        <div class="container">
            <div class="row">
                <?php contact(); ?>
            </div>
        </div>
    </section>

</div>


<?php footer(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Productos').addClass('active');
    });
    function showLightbox2() {
        document.getElementById('over2').style.display='block';
        document.getElementById('fade2').style.display='block';
    }
    function hideLightbox2() {
        document.getElementById('over2').style.display='none';
        document.getElementById('fade2').style.display='none';
    }
</script>
</body>

</html>
