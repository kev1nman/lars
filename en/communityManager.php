<?php require_once("master.php"); cabecera(); ?>

<div class="communityManager">

    <section class="sect3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="azul">Community Manager Services</h1>
                    <div class="col-md-12 plan1">
                        <div class="col-md-3">
                            <h2 class="circuloPlan">PLAN 1</h2>

                        </div>
                        <div class="col-md-9 text-left">
                            <p>
                                This package is designed for the use of a single social network (Twitter, Facebook or Instagram) in order to boost and promote your company.
                            </p>
                            <h2 class="azul">Características:</h2>
                            <h3>Creation or management of the chosen account:</h3>
                            <ul>
                                <li><b>Twitter:</b> Up to 3 daily tweets plus interactions (rt's and answers *)</li>
                                <li><b>Facebook:</b> Up to 2 interim updates.</li>
                                <li><b>Instagram:</b> Up to 2 photos per day.</li>
                            </ul>
                            <h3>Development of the campaign advertising campaign:</h3>
                            <p>Analysis of your company to determine what is going to communicate in social networks.</p>

                        </div>

                    </div>
                    <div class="col-md-12 plan2">
                        <div class="col-md-3">
                            <h2 class="circuloPlan">PLAN 2</h2>

                        </div>
                        <div class="col-md-9 text-left">
                            <p>
                                This package includes the hiring of two social networks (Twitter, Facebook and / or Instagram) in order to boost and promote your company.
                            </p>
                            <h2 class="azul">Characteristics:</h2>
                            <h3>Creation or management of the chosen account:</h3>
                            <ul>
                                <li><b>Twitter:</b> Up to 3 daily tweets plus interactions (rt's and answers *)</li>
                                <li><b>Facebook:</b> Up to 2 interim updates.</li>
                                <li><b>Instagram:</b> Up to 2 photos per day.</li>
                                <li><b>Monthly contest through social networks.</b> </li>
                            </ul>
                            <h3>Development of the campaign advertising campaign:</h3>
                            <p>Analysis of your company to determine what is going to communicate in social networks.</p>

                        </div>

                    </div>
                    <div class="col-md-12 plan1">
                        <div class="col-md-3">
                            <h2 class="circuloPlan">PLAN 3</h2>

                        </div>
                        <div class="col-md-9 text-left">
                            <p>
                                Apply for large companies that have a lot of graphic material to advertise: This package includes all social networks (Twitter, Facebook and Instagram) in order to boost and promote your company.

                            </p>
                            <h2 class="azul">Characteristics:</h2>
                            <h3>Creation or management of the chosen account:</h3>
                            <ul>
                                <li><b>Create or manage the Twitter account:</b> Up to 4 daily tweets plus iterations (rt's and answers *) </li>
                                <li><b>Create or manage the Facebook account:</b> Up to 3 interim updates. </li>
                                <li><b>Creation or management of the Instagram account:</b> Up to 3 photos daily, including advertising to other pages which increases the followers.</li>
                            </ul>
                            <h3>Campaign Development Campaign:</h3>
                            <p>Analysis of your company to determine what is going to communicate in social networks.</p>
                            <h3>Monthly contest through social networks. </h3>
                        </div>

                    </div>
                </div>
                <div class="col-xs-12">
                    <a href="javascript:showLightbox();" id='COMMUNITY MANAGER' class="click btn btn-primary btn-lg botonAzul">¡ REQUEST QUOTE !</a>
                </div>
            </div>
        </div>
    </section>


    <section class="contact">
        <div class="container">
            <div class="row">
                <?php contact(); ?>
            </div>
        </div>
    </section>

</div>



<?php footer(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Servicios').addClass('active');
    });
</script>
</body>

</html>
