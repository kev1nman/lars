<?php require_once("master.php"); cabecera(); ?>
<div class="productos">

    <section class="sect3">
        <div class="container">
            <div class="row">
                <div class="col-md-6 plan2">
                    <h1>Activity Monitor</h1>
                    <p>
                        Beneficios como ver múltiples ordenadores en su red al mismo tiempo, generar informes, programar captures de pantalla, Registrar las pulsaciones de teclado en aplicaciones, correos electrónicos, sitios web y chats son lo que este software de última generación
                        te ofrece, solicita tu licencia ya!
                    </p>
                    <a href="activityMonitor.php" class="btn btn-primary btn-lg botonAzul">¡ VER MAS !</a>
                </div>
                <div class="col-md-6 plan1">
                    <h1>Blaster Analitycs</h1>
                    <p>
                        Blaster Analityc Es la herramienta desarrollada en Colombia de análisis web que le permitirá rastrear las acciones de sus destinatarios una vez que ingresen a su +pagina web y le otorgará las respuestas a sus preguntas, Usted puede determinar estadísticas
                        básicas tales como el número de sus visitantes web, su origen geográfico, o el tiempo de permanencia, y muchas otras informaciones importantes.
                    </p>
                    <a href="blasterAnalytics.php" class="btn btn-primary btn-lg botonAzul">¡ VER MAS !</a>
                </div>
            </div>
        </div>
    </section>

    <section class="contact">
        <div class="container">
            <div class="row">
                <?php contact(); ?>
            </div>
        </div>
    </section>

</div>

<?php footer(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Productos').addClass('active');
    });
</script>
</body>

</html>
