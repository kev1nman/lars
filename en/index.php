<?php require_once("master.php"); cabecera(); ?>
<head>
  <!--Start of Zendesk Chat Script-->
			<script type="text/javascript">
			window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
			d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
			$.src="//v2.zopim.com/?43bVlFst2la5AkPi1ywevcXcic3UTyPR";z.t=+new Date;$.
			type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
			</script>
			<!--End of Zendesk Chat Script-->
</head>
<section class="sect2">
    <div class="corte">
        <ul class="rslides" id="serviciosSlider">
            <li>
                <div class="slide1">
                    <div class="textSlide">
                        <h2>
                        ¡IN 10 DAYS!
                    </h2>
                        <h3>
                        We Create your Website quick and easy!
                    </h3>
                        <div class="precio">
                            <h2 class="caviar">
                                Since 50 USD/Month
                            </h2>
                        </div>
                        <a href="sitioWeb.php#sitioWeb" class="btn btn-primary btn-lg botonWhite">¡ SEE MORE !</a>

                    </div>
                </div>
            </li>
            <li>
                <div class="slide2">
                    <div class="textSlide">
                        <h2>
                            Are u Ready for sell in Internet?
                        </h2>
                        <h3>
                            Only in 10 days you can start your digital bussiness, we are ready for any challenge, we perfectly understand the actual market exigences, YOUR COSTUMERS ARE WAITING FOR YOU!
                        </h3>
                        <div class="precio">
                            <h2 class="caviar">
                                Since 300 USD
                            </h2>
                        </div>
                        <a href="sitioWeb.php#ecommerce" class="btn btn-primary btn-lg botonWhite">¡ I WANT TO START NOW !</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="slide3">
                    <div class="textSlide">
                        <h2>
                        How to Sell in Facebook?
                    </h2>
                        <h3>
                        Sell on Facebook revolutioned, how the thinks are made, our specialists in Communty Manager, want to show you how you can do it.
                    </h3>
                        <div class="precio">
                            <h2 class="caviar">
                                Since 150 USD/Month
                            </h2>
                        </div>
                        <a href="communityManager.php" class="btn btn-primary btn-lg botonWhite">¡ SEE MORE !</a>

                    </div>
                </div>
            </li>
            <li>
                <div class="slide4">
                    <div class="textSlide">
                        <h2>
                        ¡WE DEVELOP YOUR IMAGE!
                    </h2>
                        <h3>
                        Graphic design, in high performance on palm your hands, we shape to your ideas and put it in producction.
                    </h3>
                        <div class="precio">
                            <h2 class="caviar">
                                Since 150 USD
                            </h2>
                        </div>
                        <a href="disenoGrafico.php" class="btn btn-primary btn-lg botonWhite">¡ SEE MORE !</a>

                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>


<section class="sect3">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4 col-md-offset-2">
                <img src="img/phone.png" alt="">
            </div>
            <div class="ccol-xs-12 col-md-4 quienesSomos">
                <h1 class="">ABOUT US</h1>
                <p>
					We are the perfect option for your bussiness development, As your tech partner, we to listen you and make real your ideas, We are a crew of professionals with more than 8 years of experience in web/mobile development app and digital maketing. <br>
					WE WORK WITH YOU, hand to hand, to give shape and color to your projects, we dont give you only a service, we want to make big your company.
                </p>
                <img class="img-responsive center-block" src="img/iconsQS.png" alt="">
            </div>
        </div>
    </div>
</section>



<section class="sect4">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1>Why work with us ?</h1>
            </div>
            <div class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <img class="img-responsive center-block" src="img/ipad.png" alt="">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 porqNosotros">
                    <div class="caja">
                        <p>
                            Our comminment with your projects define us, we dont want to give you only a service, we want to make your company successful.
                        </p>
                        <h3>YOUR SUCCESS IS OUR SUCCESS.</h3>
                    </div>
                    <div class="flecha"></div>
                    <a href="servicios.php" id='¿POR QUE TRABAJAR CON NOSOTROS?' class="click btn btn-primary btn-lg botonAzul">¡ START NOW !</a>
                </div>
            </div>
        </div>
    </div>
</section>




<section class="sect5">
    <div class="container">
        <div class="row">
            <h1 id="Servicios">Services</h1>
            <div class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="icono">
                        <a href="disenoGrafico.php">
                            <img class="img-responsive icon1" src="img/servicios/1.png" alt="">
                            <img class="img-responsive icon11" src="img/servicios/11.png" alt="">
                        </a>
                    </div>
                    <div class="iconoTexto">
                        <a href="disenoGrafico.php">
                            <h4>Graphic Desing</h4>
                        </a>
                        <p>
                            -	Brand Consultant <br>
                            -	Logo Dseing<br>
                            -	POP porducts (Up to 10k)<br>


                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="linea desaparece768">
                        <div class="punta puntaL"></div>
                        <div class="punta puntaR"></div>
                    </div>
                    <div class="icono">
                        <a href="sitioWeb.php#sitioWeb">
                            <img class="img-responsive icon1" src="img/servicios/2.png" alt="">
                            <img class="img-responsive icon11" src="img/servicios/21.png" alt="">
                        </a>
                    </div>
                    <div class="iconoTexto">
                        <a href="sitioWeb.php#sitioWeb">
                            <h4>Webistes</h4>
                        </a>
                        <p>
                            Amazing webisted in <b>RECORD TIME!</b>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="linea desaparece">
                        <div class="punta puntaL"></div>
                        <div class="punta puntaR"></div>
                    </div>
                    <div class="icono">
                        <a href="sitioWeb.php#aplicacionesMoviles">
                            <img class="img-responsive icon1" src="img/servicios/3.png" alt="">
                            <img class="img-responsive icon11" src="img/servicios/31.png" alt="">
                        </a>
                    </div>
                    <div class="iconoTexto">
                        <a href="sitioWeb.php#aplicacionesMoviles">
                            <h4>Mobile Apps</h4>
                        </a>
                        <p>
                            iOS® and Android® applications development
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
                    <div class="linea aparece">
                        <div class="punta puntaL"></div>
                        <div class="punta puntaR"></div>
                    </div>
                    <div class="icono">
                        <a href="activityMonitor.php">
                            <img class="img-responsive icon1" src="img/servicios/4.png" alt="">
                            <img class="img-responsive icon11" src="img/servicios/41.png" alt="">
                        </a>
                    </div>
                    <div class="iconoTexto">
                        <a href="activityMonitor.php">
                            <h4>Employee Monitoring</h4>
                        </a>
                        <p>
                            See remote desktops of anybody in your local network.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="linea desaparece">
                        <div class="punta puntaL"></div>
                        <div class="punta puntaR"></div>
                    </div>
                    <div class="icono">
                        <a href="communityManager.php">
                            <img class="img-responsive icon1" src="img/servicios/5.png" alt="">
                            <img class="img-responsive icon11" src="img/servicios/51.png" alt="">
                        </a>
                    </div>
                    <div class="iconoTexto">
                        <a href="communityManager.php">
                            <h4>Community Manager</h4>
                        </a>
                        <p>
                            We manage all your social netwoks with low costs, strategy and creativity.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="contact">
    <div class="container">
        <div class="row">
            <?php contact(); ?>
        </div>
    </div>
</section>





<?php footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#Home').addClass('active');
    });
</script>

</body>

</html>
