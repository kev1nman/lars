<?php require_once("master.php"); cabecera(); ?>
<!--Start of Zendesk Chat Script-->
			<script type="text/javascript">
			window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
			d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
			$.src="//v2.zopim.com/?43bVlFst2la5AkPi1ywevcXcic3UTyPR";z.t=+new Date;$.
			type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
			</script>
			<!--End of Zendesk Chat Script-->
<div class="nosotros">

    <section class="sect3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="azul">¡Your Vision is our priority!</h1>
                    <div class="col-xs-12 col-md-6">
                        <p>
                             We are the perfect option to your bussiness development, As their your tech partner, to listen you and make real your ideas, We are a crew of professionals with more than 8 years of experience in web/mobile apps development and digital maketing.</br>

                    WE WORK WITH YOU, hand to hand to give shape and color to your projects, we do dont want to give you only a service, we want to make big your company.
                        </p>

                    </div>
                    <div class="col-xs-12 col-md-6">
                        <img class="img-responsive center-block img-thumbnail" src="img/vision.jpg" alt="">
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="sect4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h1>¡OUR MISSION!</h1>
                </div>
                <div class="col-md-12">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <img class="img-responsive center-block img-circle" src="img/mision.jpg" alt="">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <p>
                            To be the key impulse for the business and technological development of our clients, offering creative and innovative solutions, that accompany them on their way to success; Through our efficient and quality services with satisfied customers in more than 5 countries.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>



</div>

<section class="contact contact3">
    <div class="container">
        <div class="row">
            <?php contact(); ?>
        </div>
    </div>
</section>


<?php footer(); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('#Nosotros').addClass('active');
    });
</script>
</body>
</html>
