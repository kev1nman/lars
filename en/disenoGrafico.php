<?php require_once("master.php"); cabecera(); ?>
<div class="disenoGrafico">

    <section class="sect3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>
Graphic design</h1>
                    <p class="text-justify">
                        At LARS we know how important an image is for a company or brand, that's why we have a team of highly trained professionals to give you an adequate and functional graphic identity to your products, with our design service graphic we can offer you help on the theme of: logos, digital advertising, corporate identity, brochures and much more ...
                    </p>

                </div>
                <div class="col-xs-12">
                    <a href="javascript:showLightbox();" id='DISEÑO GRÁFICO' class="click btn btn-primary btn-lg botonAzul">¡ I'M INTERESTED !</a>
                </div>
            </div>
        </div>
    </section>




    <section class="servicios">
        <div class="container-fluid">
            <div class="row">
                <div class="servicio1">
                    <div class="textServicio">
                        <h2>
                        ¡WE DEVELOP YOUR IMAGE!
                    </h2>
                        <h3>
                        Graphic design, in high performance on palm your hands, we shape to your ideas and put it in producction.
                    </h3>
                        <div class="precio">
                            <h2 class="caviar">
                                Since 150 USD
                            </h2>
                        </div>
                        <a href="javascript:showLightbox();" id='DISEÑO GRÁFICO' class="click btn btn-primary btn-lg botonWhite">¡ I'M INTERESTED !</a>

                    </div>
                </div>

            </div>
        </div>
    </section>

</div>

<section class="contact contact2">
    <div class="container">
        <div class="row">
            <?php contact(); ?>
        </div>
    </div>
</section>


<?php footer(); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Servicios').addClass('active');
    });
</script>
</body>
</html>
