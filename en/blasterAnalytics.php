<?php require_once("master.php"); cabecera(); ?>

<div class="blasterAnalitycs">

    <section class="sect4">
        <div class="container-fluid">
            <div class="row">
                <h1>Blaster Analitycs</h1>
                <div class="col-xs-12 text-center">
                    <div class="textMonitor">
                        <h2>What is it?</h2>
                        <p>Blaster Analityc It is the tool developed in Colombia web analytics that will allow you to track the actions of your recipients once they enter your web page and will give you the answers to your questions, You can determine basic statistics such as the number of your web visitors , Geographical origin, or length of stay, and many other important information.
                        </p>
                        <a class="btn btn-info btnMonitor" href="#blasterAnalitycs"><span class="glyphicon glyphicon-chevron-down"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="sect3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 id="blasterAnalitycs" class="azul">Easy and Intuitive</h1>
                    <p>
                        Our designers and developers found a way to make complex, EASY. Blaster Analytics is so incredibly intuitive that you will not need manuals or tedious courses to use it.
                    </p>
                    <h1 class="azul">Does it have any cost?</h1>
                    <p>
                         Yes, Blaster Analytics has a cost, and this varies depending on the mode of use you choose.
                    </p>
                    <h1 class="azul">On site</h1>
                    <p>
                        An Activity Monitor server is installed on your premises and configured in such a way that your IT administrators can manage it. (Higher initial cost)
                    </p>
                    <h1 class="azul">How cloud service</h1>
                    <p>
                        We will provide access credentials for you and your authorized users, to access the Internet and perform the analysis you want, in addition you will have an Activity Monitor LOG backup on our powerful servers. (Lower initial cost).
                    </p>
                </div>
                <div class="col-xs-12">
                    <a href="javascript:showLightbox();" id='BLASTER ANALITYCS' class="click btn btn-primary btn-lg botonAzul">¡ I'M INTERESTED !</a>
                </div>
            </div>
        </div>
    </section>


    <section class="contact">
        <div class="container">
            <div class="row">
                <?php contact(); ?>
            </div>
        </div>
    </section>

</div>



<?php footer(); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Productos').addClass('active');
    });
</script>
</body>
</html>
