<?php
error_reporting(E_ALL ^ E_NOTICE);
    function cabecera(){echo "
        <!DOCTYPE html>
        <html lang='en'>
        <head>
            <meta charset='UTF-8'>
            <meta name='viewport' content='widtr=device-widtr, initial-scale=1.0'>
            <meta http-equiv='X-UA-Compatible' content='ie=edge'>

                <!--bootstrap CSS-->
                    <link rel='stylesheet' href='css/bootstrap.min.css'>
                <!--iconStyle-->
                    <link rel='stylesheet' href='css/iconStyle.css'>
                <!--responsiveSlides-->
                    <link rel='stylesheet' href='css/responsiveslides.css'>
                <!--sly-->
                    <link rel='stylesheet' href='css/sly.css'>
                <!--Animate-->
                    <link rel='stylesheet' href='css/animate.min.css'>
                <!--Estilos-->
                    <link rel='stylesheet' href='css/style.css'>


                <!--jquery-->
                    <script src='js/jquery.js' type='text/javascript'></script>
                <!--bootstrap-->
                    <script src='js/bootstrap.min.js' type='text/javascript'></script>
                <!--viewportchecker-->
                    <script src='js/jquery.viewportchecker.js' type='text/javascript'></script>





                    <script>
                      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                      ga('create', 'UA-91542307-1', 'auto');
                      ga('send', 'pageview');

                    </script>




            <title>Lars Software</title>
        </head>
        <body>
            <nav class='navbar navbar-inverse navbar-static-top'>
                <div class='container'><!--nav.container -->
                    <div class='navbar-header'>
                        <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar' aria-expanded='false' aria-controls='navbar'>
                            <span class='sr-only'>Toggle navigation</span>
                            <span class='icon-bar'></span>
                            <span class='icon-bar'></span>
                            <span class='icon-bar'></span>
                        </button>
                    </div>

                    <!--nav -->
                    <div id='navbar' class='navbar-collapse collapse'>
                        <!--left -->
                        <ul class='nav navbar-nav'>
                            <li><a id='Home' href='index.php'>Home</a></li>
                            <li><a id='Nosotros' href='nosotros.php'>Nosotros</a></li>
                            <li class='dropdown'>
                                <span class='dropdown-toggle plus' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>+</span>
                                <a id='Servicios' href='servicios.php' class='dropdown-toggle disabled' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Servicios <span class='caret'></span></a>
                                <ul class='dropdown-menu animated fadeInDown'>
                                    <li><a href='sitioWeb.php#sitioWeb'>PÁGINAS WEB</a></li>
                                    <li><a href='sitioWeb.php#aplicacionesWeb'>APLICACIONES WEB</a></li>
                                    <li><a href='sitioWeb.php#aplicacionesMoviles'>APLICACIONES MÓVILES</a></li>
                                    <li><a href='communityManager.php'>COMMUNITY MANAGER</a></li>
                                    <li><a href='disenoGrafico.php'>DISEÑO GRAFICO</a></li>
                                </ul>
                            </li>
                            <!--
                            <li class='dropdown'>
                                <span class='dropdown-toggle plus' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>+</span>
                                <a id='Productos' href='productos.php' class='dropdown-toggle disabled' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Productos <span class='caret'></span></a>
                                <ul class='dropdown-menu'>
                                    <li><a href='activityMonitor.php'>Activity Monitor</a></li>
                                    <li><a href='blasterAnalytics.php'>Blaster Analitycs</a></li>
                                </ul>
                            </li>
                            -->
                            <li><a id='CONTACTO' class='click' href='javascript:showLightbox();'>Contácto</a></li>
                        </ul><!--/.left -->
                    </div><!--/.nav-collapse -->
                </div><!--/.nav.container -->
            </nav>

            <!-- sect1 -->
            <section class='sect1'>
                <!-- CONTAINER -->
                <div class='container'>
                    <div class='row'>
                        <div class='col-md-12 text-center'>
                            <a href='index.php'>
                                <img class='img-responsive center-block animated fadeInDown' src='img/lars.png' alt=''>
                                <img class='img-responsive center-block animated fadeInUp' src='img/softwareCompany.png' alt=''>
                            </a>
                            <a href='javascript:showLightbox();' id='CUÉNTANOS TU IDEA' class='click btn btn-primary btn-lg botonAzul'>¡ CUÉNTANOS TU IDEA !</a>
                        </div>
                    </div>
                </div>
                <div class='precio invert'>
                    <h2></h2>
                </div>
                <!-- FIN / CONTAINER -->
            </section>
            <!-- FIN / sect1 -->


            <!-- Call to action -->
            <div id='over' class='overbox animated fadeInDown'>
                <div class='contacto'>
                    <div class='contactoForm'>
                        <h1>¡SOLO UN PASO MAS!</h1>
                        <p>Déjanos tus datos para que nuestro equipo de desarrollo se ponga en contacto contigo y así poder darle forma y color a tu idea.</p>


                        <form class='form-horizontal' id='form_index_servicios' data-toggle='validator' role='form'>


                            <!-- Text input-->
                            <input type='hidden' name='click' id='click' value=''/>

                            <div class='col-md-6'>
                                <!-- Text input-->
                                <div class='form-group'>
                                    <div class='col-md-12'>
                                        <input id='nombre' name='nombre2' type='text' placeholder='Nombre' class='form-control input-lg' required>
                                        <span class='icon-user icon'></span>
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class='form-group'>
                                    <div class='col-md-12'>
                                        <input id='apellido' name='apellido2' type='text' placeholder='Apellido' class='form-control input-lg' required>
                                        <span class='icon-user icon'></span>
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class='form-group'>
                                    <div class='col-md-12'>
                                        <input id='correo' name='correo2' type='email' placeholder='Correo' class='form-control input-lg' data-error='Correo inválido, ejemplo: email@mail.com' required>
                                        <span class='icon-envelop icon'></span>
                                        <div class='help-block witr-errors'></div>
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class='form-group'>
                                    <div class='col-md-12'>
                                        <input id='telefono' name='telefono2' type='text' placeholder='Teléfono' class='form-control input-lg' required>
                                        <span class='icon-mobile icon'></span>
                                    </div>
                                </div>

                            </div>
                            <div class='col-md-6'>

                                <!-- Text input-->
                                <div class='form-group'>
                                    <div class='col-md-12'>
                                        <input id='pais' name='pais' type='text' placeholder='Pais' class='form-control input-lg' required>
                                        <span class='icon-user icon'></span>
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class='form-group'>
                                    <div class='col-md-12'>
                                        <input id='empresa' name='empresa' type='text' placeholder='Empresa' class='form-control input-lg' required>
                                        <span class='icon-user icon'></span>
                                    </div>
                                </div>


                                <!-- Textarea -->
                                <div class='form-group'>
                                    <div class='col-md-12'>
                                        <textarea class='form-control input-lg' id='idea' name='mensaje2' placeholder='¡Cuéntanos tu idea!' required></textarea>
                                    </div>
                                </div>
                            </div>


                            <!-- Button -->
                            <div class='form-group'>
                                <div class='col-md-12'>
                                    <button id='enviar' type='submit' name='enviar' class='btn btn-primary btn-lg enviarBtn2' data-toggle='modal' data-target='#myModal'>Enviar</button>
                                    <a href='javascript:hideLightbox();' class='linkWhite'>Cerrar</a>
                                </div>
                            </div>





                        </form>

                    </div>
                    <div class='precioAzul'>
                        <div class='col-md-6'>
                            <span class='icon-mobile'></span>
                            <b>(+57)</b> 300 345 91 65
                        </div>
                        <div class='col-md-6'>
                            <span class='icon-mobile'></span>
                            <b>(+571)</b> 682 9729
                        </div>
                    </div>
                </div>
            </div>
            <div id='fade' class='fadebox'>&nbsp;</div>
            <!-- Call to action -->



            <!-- MONITOR ACTIVITY -->
            <div id='over2' class='overbox'>
                <div class='contacto comprarMonitor'>
                    <form class='form-horizontal' id='' data-toggle='' role='form'>
                        <div class='col-md-12'>
                            <div class='col-md-12'>
                                <h1>¡MONITOR ACTIVITY!</h1>
                                <div class='table-responsive'>
                                    <table class='table table-condensed'>
                                        <thead class='row header blue'>
                                            <tr>
                                                <td colspan='2'>Paquete</td>
                                                <td>Cantidad</td>
                                                <td>Precio por unidad</td>
                                                <td>Total</td>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td class='text-right'>Activity monitor Enterprise ®</td>
                                                <td class='text-left'>
                                                    <select class='form-control licencias' name=''>
                                                        <option value=''>3 licencias de ordenador</option>
                                                        <option value=''>6 licencias de ordenador</option>
                                                        <option value=''>12 licencias de ordenador</option>
                                                        <option value=''>25 licencias de ordenador</option>
                                                        <option value=''>50 licencias de ordenador</option>
                                                        <option value=''>100 licencias de ordenador</option>
                                                        <option value=''>200 licencias de ordenador</option>
                                                    </select>
                                                </td>
                                                <td></td>
                                                <td>$189.95</td>
                                                <td>$189.95</td>
                                            </tr>

                                            <tr>
                                                <td colspan='2'>Licencias adicionales</td>
                                                <td>
                                                    <div class='input-group number'>
                                                        <span class='input-group-addon icon-key'></span>
                                                        <input type='number' min='0' max='100' class='form-control' placeholder=''>
                                                    </div>
                                                </td>
                                                <td>$189.95</td>
                                                <td>$189.95</td>
                                            </tr>

                                            <tr class='success'>
                                                <td colspan='3'></td>
                                                <td>Total</td>
                                                <td class='success'>$189.95</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class='col-md-6'>
                                    <div class='table-responsive'>
                                        <table class='table table-condensed'>
                                            <thead class='row header blue'>
                                                <tr>
                                                    <td>Información de la factura</td>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr><td><!-- Text input-->
                                                    <div class='input-group'>
                                                        <span class='input-group-addon icon-office'></span>
                                                        <input id='compañia' name='compañia' type='text' placeholder='Nombre de la compañia' class='form-control'>
                                                    </div>
                                                </td></tr>

                                                <tr><td><!-- Text input-->
                                                    <div class='input-group'>
                                                        <span class='input-group-addon icon-user'></span>
                                                        <input id='nombre3' name='nombre3' type='text' placeholder='Nombre' class='form-control'>
                                                    </div>

                                                </td></tr>

                                                <tr><td><!-- Text input-->
                                                    <div class='input-group'>
                                                        <span class='input-group-addon icon-user'></span>
                                                        <input id='apellido3' name='apellido3' type='text' placeholder='Apellido' class='form-control'>
                                                    </div>
                                                </td></tr>

                                                <tr><td><!-- Text input-->
                                                    <div class='input-group'>
                                                        <span class='input-group-addon icon-compass2'></span>
                                                        <select class='form-control' name='pais3'>
                                                            <option value=''>Colombia</option>
                                                            <option value=''>Panamá</option>
                                                            <option value=''>Venezuela</option>
                                                            <option value=''>Peru</option>
                                                        </select>
                                                    </div>

                                                </td></tr>

                                                <tr><td><!-- Text input-->
                                                    <div class='input-group'>
                                                        <span class='input-group-addon icon-envelop'></span>
                                                        <input id='correo3' name='correo3' type='text' placeholder='Correo' class='form-control'>
                                                    </div>

                                                </td></tr>

                                                <tr><td><!-- Text input-->
                                                    <div class='input-group'>
                                                        <span class='input-group-addon icon-envelop'></span>
                                                        <input id='correo33' name='correo33' type='text' placeholder='Confirmar correo' class='form-control'>
                                                    </div>

                                                </td></tr>

                                                <tr><td><!-- Text input-->
                                                    <div class='input-group'>
                                                        <span class='input-group-addon icon-whatsapp'></span>
                                                        <input id='whatsapp' name='whatsapp' type='text' placeholder='Whatsapp' class='form-control'>
                                                    </div>

                                                </td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class='col-md-6'>
                                    <div class='table-responsive'>
                                        <table class='table table-condensed'>
                                            <thead class='row header blue'>
                                                <tr>
                                                    <td>Información de la factura</td>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr><td><!-- Text input-->
                                                    <div class='input-group'>
                                                        <span class='input-group-addon icon-key'></span>
                                                        <input id='numTarjeta' name='numTarjeta' type='text' placeholder='Numero de tarjeta' class='form-control'>
                                                    </div>
                                                </td></tr>

                                                <tr><td><!-- Text input-->
                                                    <div class='col-md-12'>
                                                        <h5>Tipo de tarjeta</h5>
                                                    </div>
                                                    <div class=''>
                                                        <img src='img/pago/visa.jpg' class='img-responsive left' alt=''>
                                                        <img src='img/pago/master.jpg' class='img-responsive left' alt=''>
                                                        <img src='img/pago/american.jpg' class='img-responsive left pagoSelected' alt=''>
                                                        <img src='img/pago/cb.jpg' class='img-responsive left' alt=''>
                                                        <img src='img/pago/diners.jpg' class='img-responsive left' alt=''>
                                                        <img src='img/pago/discover.jpg' class='img-responsive left' alt=''>
                                                        <img src='img/pago/jdb.jpg' class='img-responsive left' alt=''>
                                                    </div>
                                                </td></tr>

                                                <tr><td><!-- Text input-->
                                                    <div class='col-md-12 noPadding'>
                                                        <h5>Fecha de vencimiento</h5>
                                                    </div>
                                                    <div class='col-xs-6'>
                                                        <div class='input-group'>
                                                            <span class='input-group-addon'>Mes</span>
                                                            <input id='mm' name='mm' type='text' placeholder='MM' class='form-control text-center'>
                                                        </div>
                                                    </div>
                                                    <div class='col-xs-6'>
                                                        <div class='input-group'>
                                                            <span class='input-group-addon'>Año</span>
                                                            <input id='aaaa' name='aaaa' type='text' placeholder='AAAA' class='form-control text-center'>
                                                        </div>
                                                    </div>
                                                </td></tr>

                                                <tr><td><!-- Text input-->
                                                    <div class='col-xs-6 noPadding'>
                                                        <h5>Código de seguridad</h5>
                                                    </div>
                                                    <div class='col-xs-6'>
                                                        <input id='codigo' name='codigo' type='text' placeholder='###' class='form-control text-center'>
                                                    </div>
                                                </td></tr>



                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class='compraExitosa'>
                                    <div class='col-md-12'>
                                        <div class='text-center compraContent'>
                                            <div class='circuloCheck'>
                                                <span class='icon-checkmark'></span>
                                            </div>
                                            <h3>¡Compra exitosa!</h3>
                                            <p>Recibiras un correo con la información sobre tu compra</p>
                                        </div>
                                    </div>
                                </div>


                                <!-- Button -->
                                <div class='form-group'>
                                    <div class='col-md-12'>
                                        <button id='enviar' type='submit' name='enviar' class='btn btn-primary btn-lg enviarBtn2' data-toggle='modal' data-target='#myModal'>Enviar</button>
                                        <a href='javascript:hideLightbox2();' class='linkWhite'>Cerrar</a>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </form>
                </div>
            </div>
            <div id='fade2' class='fadebox'>&nbsp;</div>
            <!-- MONITOR Activity -->

    ";}

    function contact(){
        echo"
            <div class='col-md-12'>
                <h1>Contáctanos</h1>
            </div>
            <div class='col-md-12'>
                <div class='col-xs-12 col-sm-6'>
                    <div class='logoContact'>
                        <img class='img-responsive invert brightness center-block' src='img/lars.png' alt=''>
                        <img class='img-responsive invert brightness center-block' src='img/softwareCompany.png' alt=''>
                        <a href='https://www.facebook.com/larssoftware/' target='_blank' class='btn btn-primary btn-lg circuloRedes facebook'><span class='icon-facebook'></span></a>
                        <a href='https://www.instagram.com/larssoftwarecompany/' target='_blank' class='btn btn-primary btn-lg circuloRedes instagram'><span class='icon-instagram'></span></a>
                    </div>
                </div>
                <div class='col-xs-12 col-sm-6'>
                    <div class='contactForm'>
                        <form class='form-horizontal' data-toggle='validator' role='form' metrod='POST' id='form_index_contact'>
                            <!-- Text input-->
                            <div class='form-group'>
                                <div class='col-md-12'>
                                    <input id='nombre' name='nombre' type='text' placeholder='Nombre' class='form-control input-lg' required>
                                    <span class='icon-user icon'></span>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class='form-group'>
                                <div class='col-md-12'>
                                    <input id='telf' name='telf' type='text' placeholder='Teléfono' class='form-control input-lg' required>
                                    <span class='icon-mobile icon'></span>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class='form-group'>
                                <div class='col-md-12'>
                                    <input id='correo' name='correo' type='email' placeholder='Correo' class='form-control input-lg' data-error='Correo inválido, ejemplo: email@mail.com' required>
                                    <span class='icon-envelop icon'></span>
                                    <div class='help-block witr-errors'></div>
                                </div>
                            </div>

                            <!-- Textarea -->
                            <div class='form-group'>
                                <div class='col-md-12'>
                                    <textarea class='form-control input-lg' id='mensaje' name='mensaje' placeholder='Mensaje' required></textarea>
                                </div>
                            </div>

                            <!-- Button -->
                            <div class='form-group'>
                                <div class='col-md-4'>
                                    <button id='enviar' type='submit' name='enviar' class='btn btn-primary btn-lg enviarBtn' data-toggle='modal' data-target='#myModal'>Enviar</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>

    ";}

    function footer(){
        echo "
        <!-- Modal -->
        <div class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
            <div class='modal-dialog' role='document'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                    </div>
                    <div class='modal-body'>
                        <h3 id='respuesta' style='display: none;'></h3>
                        <h3 id='respuestaServicios' style='display: none;'></h3>
                    </div>
                    <div class='modal-footer'>
                        <button id='ok' type='button' class='btn btn-success btn-lg' data-dismiss='modal'>OK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <footer>
            <div class='container post'>
                <div class='col-md-12 text-center mapaSitio'>
                    <div class='col-md-4'><span class='icon-map'></span> Av Jimenez # 5 - 43 Bogotá</div>
                    <div class='col-md-4'><span class='icon-envelop'></span> contacto@lars.com.co</div>
                    <div class='col-md-4'><span class='icon-mobile'></span> <b>(+57)</b> 300 345 91 65 <br><b>(+571)</b> 682 9729 </div>
                </div>
            </div>
            <div class='footer'>
                <div class='container text-center'>
                    <div class='col-md-12'>
                        <p class='text-muted'>Copyright © Lars Software Company 2017</p>

                    </div>
                </div>
            </div>
        </footer>


        <!--responsiveslides-->
            <script src='js/responsiveslides.min.js' type='text/javascript'></script>
        <!--SLY - Plugin Slider-->
            <script src='js/plugins.js' type='text/javascript'></script>
        <!--SLY - sly Slider-->
            <script src='js/sly.min.js' type='text/javascript'></script>
        <!--SLY - horizontal Slider-->
            <script src='js/horizontal.js' type='text/javascript'></script>
        <!--Validator bootstrap-->
            <script src='js/validator.js' type='text/javascript'></script>

            <script type='text/javascript'>
                //Animaciones en post
            	$(document).ready(function() {
            		$('.post').addClass('hidden').viewportChecker({
            			classToAdd: 'visible animated fadeInLeft', // Class to add to the elements when they are visible
            			offset: 100
            		});
            	});
                // Menu desplegable con Hover
                $(function(){
                    $('.dropdown').hover(function() {
                        $(this).addClass('open');
                    },
                    function() {
                        $(this).removeClass('open');
                    });
                });
                // Slideshow 1
                $('#serviciosSlider').responsiveSlides({
                    timeout: 10000,
                    speed: 400,
                    nav: true,
                });
                function showLightbox() {
                    document.getElementById('over').style.display='block';
                    document.getElementById('fade').style.display='block';
                }
                function hideLightbox() {
                    document.getElementById('over').style.display='none';
                    document.getElementById('fade').style.display='none';
                }
                //Valor de botones para el correo de servicios
            	$('.click').click(function(){
                    var idBoton = $(this).attr('id');
                    $('#click').attr('value',idBoton);
            	});

                // Formulario CONTACTANOS
                $(document).on('click', '.enviarBtn', function(e) {
                    e.preventDefault();
                    var url = 'mail-it.php';

                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: $('#form_index_contact').serialize(),
                        success: function(data) {
                            console.log(data);
                            $('#respuesta').html(data);
                            $('#respuesta').slideDown();
                            $('#respuesta2').modal('show');
                            document.getElementById('form_index_contact').reset();
                        }
                    });
                });


                // Formulario SERVICIOS, PRODUCTOS, Boton CONTACTO en el menu y CUENTANOS TU IDEAA
                $(document).on('click', '.enviarBtn2', function(e) {
                    e.preventDefault();
                    var url = 'mail-it-servicios.php';

                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: $('#form_index_servicios').serialize(),
                        success: function(data) {
                            console.log(data);
                            $('#respuestaServicios').html(data);
                            $('#respuestaServicios').slideDown();
                            //$('#respuestaServicios2').modal('show');
                            document.getElementById('form_index_servicios').reset();
                        }
                    });
                });

                //Valor de botones para el correo de servicios
            	$('#ok').click(function(){
                    $('#respuesta').hide();
                    $('#respuestaServicios').hide();
            	});





            </script>

    ";}

 ?>
