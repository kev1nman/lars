<?php require_once("master.php"); cabecera(); ?>
<!--Start of Zendesk Chat Script-->
			<script type="text/javascript">
			window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
			d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
			$.src="//v2.zopim.com/?43bVlFst2la5AkPi1ywevcXcic3UTyPR";z.t=+new Date;$.
			type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
			</script>
			<!--End of Zendesk Chat Script-->
<div class="nosotros">

    <section class="sect3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="azul">¡Tu visión es nuestra prioridad!</h1>
                    <div class="col-xs-12 col-md-6">
                        <p>
                            Somos la opción ideal para tu desarrollo empresarial. Como tu aliado en tecnología, escuchamos y hacemos realidad tus ideas, somos un equipo de profesionales con más de 8 años de experiencia en el mercado digital y el desarrollo de aplicaciones web y móviles. Trabajamos CONTIGO, mano a mano para darle forma y color a tu proyecto, no solamente queremos prestarte un servicio, queremos ayudarte a alcanzar tus objetivos.
                        </p>

                    </div>
                    <div class="col-xs-12 col-md-6">
                        <img class="img-responsive center-block img-thumbnail" src="img/vision.jpg" alt="">
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="sect4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h1>¡NUESTRA MISIÓN!</h1>
                </div>
                <div class="col-md-12">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <img class="img-responsive center-block img-circle" src="img/mision.jpg" alt="">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <p>
                            Ser el impulso clave para el desarrollo empresarial y tecnológico de nuestros clientes, brindando soluciones creativas e innovadoras, que los acompañen en su camino al éxito; Mediante nuestros servicios eficaces y de calidad óptima con clientes satisfechos en más de 5 países.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>



</div>

<section class="contact contact3">
    <div class="container">
        <div class="row">
            <?php contact(); ?>
        </div>
    </div>
</section>


<?php footer(); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('#Nosotros').addClass('active');
    });
</script>
</body>
</html>
