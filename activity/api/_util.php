<?php
  // Total
  function getTotal ($vars) {
    $prices = getLicenses();
    $licenses = $vars['licenses'];

    $sub1 = 0;
    foreach ($prices['licenses'] as $value) {
      if ($value['nums'] == $licenses) {
        $sub1 = $value['price_unit'];
      }
    }

    $extras = $vars['licenses_extras'];

    if ($extras == 0)
      $sub2 = 0;
    else if ($extras > 0 && $extras <= 4)
      $sub2 = $extras * $prices['extra'][0];
    else if ($extras >= 5 && $extras < 10)
      $sub2 = $extras * $prices['extra'][1];
    else if ($extras >= 10)
      $sub2 = $extras * $prices['extra'][2];
    else
      return 0;

    return ($sub1 + $sub2);
  }

  // Clean
  function clean($entry) {
	  $entry = trim($entry);
	  $entry = stripslashes($entry);
	  $entry = htmlspecialchars($entry);
	  return $entry;
  }

  // Send Mail
  function send_mail($send_to, $send_subject, $message, $data) {
    $name = $data['first_name'] . ' ' . $data['last_name'];
    $email = $data['email'];
    $message  .= ' ' . date('m-d-Y');

    foreach($data as $index => $value) {
      $message .= "\n" . $index . ': ' . $value;
    }
    
    $send_subject .= " - {$name}";

    $headers = "From: " . $email . "\r\n" .
      "Reply-To: " . $email . "\r\n" .
      "X-Mailer: PHP/" . phpversion();

    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
      mail($send_to, $send_subject, $message, $headers);
      return true;
    } else {
      return false;
    }

  }

  // Lista de precios
  function getLicenses() {
    
    $increment = .05;

    $prices = [
      3   => 189.95,
      6   => 299.95,
      12  => 499.95,
      25  => 899.95,
      50  => 1499.95,
      100 => 2399.95,
      200 => 3999.95,
    ];

    $extra = [
      65, 50, 36
    ];

    foreach ($extra as $index => $value) {
      $extra[$index] = intval($value + ($value * $increment));
    }

    $data['licenses'] = array();
    foreach ($prices as $index => $value) {
      $data['licenses'][] = [
        'nums' => $index, 'price_unit' => intval($value + ($value * $increment))
      ];
    }

    $data['extra'] = $extra;
    
    return $data;
  }

   // Response
  function response($var = []) {
      header('Content-type: application/json');
      header('HTTP/1.1 200 OK');
      echo json_encode($var);
      exit;
  }

