<?php
    require './vendor/autoload.php';
    require './_util.php';
    
    // Validate
    function validate() {
        $_POST = $_REQUEST;
        $error = [];
        $vars['company'] = clean($_POST['company']);
        /* if (!$vars['company']) {
          $error['company'] = true;
        } */

        $vars['first_name'] = clean($_POST['first_name']);
        if (!$vars['first_name']) {
          $error['first_name'] = true;
        }

        $vars['last_name'] = clean($_POST['last_name']);
        if (!$vars['last_name']) {
          $error['last_name'] = true;
        }

        $vars['country'] = clean($_POST['country']);
        if (!$vars['country']) {
          $error['exceptions'][] = 'Not country var';
        }

        $vars['email'] = clean($_POST['email']);
        if (!$vars['email']) {
          $error['email'] = true;
        }

        $vars['whatsapp'] = clean($_POST['whatsapp']);
        /* if (!$vars['whatsapp']) {
          $error['whatsapp'] = true;
        } */
        
        $vars['number'] = clean($_POST['number']);
        if (!$vars['number'] || !preg_match('/^[0-9]{12,19}$/', $vars['number'])) {
          $error['number'] = true;
        }
        
        $vars['type'] = clean($_POST['type']);
        if (!$vars['type']) {
          $error['type'] = true;
        }
        
        $vars['month'] = clean($_POST['month']);
        if (!$vars['month'] || !preg_match('/^[0-9]{2}$/', $vars['month'])) {
          $error['month'] = true;
        }
        
        $vars['year'] = clean($_POST['year']);
        if (!$vars['year'] || !preg_match('/^[0-9]{4}$/', $vars['year'])) {
          $error['year'] = true;
        }
        
        $vars['cvv'] = clean($_POST['cvv']);
        if (!$vars['cvv'] || !preg_match('/^[0-9]{3,4}$/', $vars['cvv'])) {
          $error['cvv'] = true;
        }
        
        $vars['total'] = clean($_POST['total']);
        if (!$vars['total']) {
          $error['exception'][] =  'Not total var';
        }
         
        $vars['licenses'] = clean($_POST['licenses']);
        if (!$vars['licenses']) {
          $error['exception'][] =  'Not licenses var';
        }
        
        $vars['licenses_price_unit'] = clean($_POST['licenses_price_unit']);
        if (!$vars['licenses_price_unit']) {
          $error['exception'][] =  'Not licenses price unit var';
        }
        
        $vars['licenses_extras'] = clean($_POST['licenses_extras']);
        /* if (!$vars['licenses_extras']) {
          $error['exceptions'][] =  'Not licenses extras var';
        } */
        
        $vars['licenses_extras_price_unit'] = clean($_POST['licenses_extras_price_unit']);
        /* if (!$vars['licenses_extras_price_unit']) {
          $error['exceptions'][] =  'Not licenses extras price unit var';
        } */

        // Response error validation
        if (count($error) > 0) {
          return ['error' => $error ];
        }

        return $vars;
    }

    $vars = validate();
	  // response($vars);

    if ($vars['error'])
    {
      response($vars);
    }

    // Validar el total
    $total_backend = getTotal($vars);
    if (($total_backend != $vars['total'])) {
      response(['error' => [
        'exception' => ['Problema al validar el total']
      ]]);
    }

    $apiContext = new \PayPal\Rest\ApiContext(
      new \PayPal\Auth\OAuthTokenCredential(
        'AXZ7tctFCqwpO2acPqk8060N-U8A29Zlb1T-d5mFLHZnAUa4c2-3TaOge9HI1zwSvg-CW0hxZwYeBIhZ',
        'EJin6v58eEfuSROedTQmbZpCX2cYr-UwcfciLcd0Qs0RMPicrLmtXJfRSTvLg6nRtiiy1Y_vZgnOF8Ei'
      )
    );

		/* $apiContext->setConfig(
      array(
        'mode' => 'sandbox',
      )
    ); */

    $card = new \PayPal\Api\PaymentCard();
    $card->setType($vars['type'])
      ->setNumber($vars['number'])
      ->setExpireMonth($vars['month'])
      ->setExpireYear($vars['year'])
      ->setBillingCountry("US")
      ->setCvv2($vars['cvv']);
   
    $fi = new \PayPal\Api\FundingInstrument();
    $fi->setPaymentCard($card);

    $payer = new \PayPal\Api\Payer();
    $payer->setPaymentMethod("credit_card")
      ->setFundingInstruments(array($fi));

    $item1 = new \PayPal\Api\Item();
    $item1->setName("Activity Monitor: " . ($vars['licenses'] + $vars['licenses_extras']))
      ->setDescription("Licencias")
      ->setCurrency('USD')
      ->setQuantity(1)
      ->setTax(0)
      ->setPrice($vars['total']);

    $itemList = new \PayPal\Api\ItemList();
    $itemList->setItems(array($item1));

    $details = new \PayPal\Api\Details();
    $details->setShipping(0)
      ->setTax(0)
      ->setSubtotal($vars['total']);

    $amount = new \PayPal\Api\Amount();
    $amount->setCurrency("USD")
      ->setTotal($vars['total'])
      ->setDetails($details);

    $transaction = new \PayPal\Api\Transaction();
    $transaction->setAmount($amount)
      ->setItemList($itemList)
      ->setDescription("Payment")
      ->setInvoiceNumber(uniqid());

    $payment = new \PayPal\Api\Payment();
    $payment->setIntent("sale")
      ->setPayer($payer)
      ->setTransactions(array($transaction));

    $request = clone $payment;

   try {
      $payment->create($apiContext);
   /* } catch (\PayPal\Exception\PayPalConnectionException $ex) {
   response (['error' => ['exception' => ['code' => $ex->getCode(), 'message' => $ex->getMessage(), 'data' => $ex->getData()]]]); */
   } catch (Exception $ex) {
     response (['error' => ['exception' => ['Ocurrió un problema al procesar los datos de su tarjeta de crédito.
       Revise cuidadosamente la información suministrada he intente de nuevo. Gracias!']]]);
      // response (['error' => ['exception' => ['code' => $ex->getCode(), 'message' => $ex->getMessage(), 'data' => $ex->getData()]]]);
   }

   $send_to = 'ludwig.angarita@lars.com.co';
   send_mail($send_to, 'Lars - Activity Monitor', 'Datos de compra', $vars);
   
   $send_to = $vars['email'];
   send_mail($send_to, 'Lars - Activity Monitor', 'Datos de compra', $vars);

   response(['success' => true]);
?>
