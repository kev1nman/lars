<?php

function response($var) {
  header('Content-type: application/json');
  header('HTTP/1.1 200 OK');
  echo json_encode($var);
  exit;
}

$send_to = "e2fletcher@gmail.com";
//$send_to = "ludwig.angarita@lars.com.co";
$send_subject = "LARS - CONTACTO desde la pag web";

$email = cleanupentries($_POST['email']);
$name = cleanupentries($_POST['first_name']) . ' ' . cleanupentries($_POST['last_name']);

if(!$email) {
  response([
    'error' => ['email' => true]
  ]);
} elseif (!cleanupentries($name)) {
  response([
    'error' => ['first_name' => true]
  ]);
}

$message = "Este correo fue enviado el " . date('m-d-Y');
foreach($_REQUEST as $index => $value) {
	$message .= "\n" . $index . ': ' . cleanupentries($value);
}

function cleanupentries($entry) {
	$entry = trim($entry);
	$entry = stripslashes($entry);
	$entry = htmlspecialchars($entry);
	return $entry;
}

$send_subject .= " - {$name}";

$headers = "From: " . $email . "\r\n" .
  "Reply-To: " . $email . "\r\n" .
  "X-Mailer: PHP/" . phpversion();

if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
  mail($send_to, $send_subject, $message, $headers);
} else {
	response(['error' => ['mail' => true]]);
}
?>
