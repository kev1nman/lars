<?php require_once("master.php"); cabecera(); ?>
<div class="disenoGrafico">

    <section class="sect3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Diseño gráfico</h1>
                    <p class="text-justify">
                        En LARS sabemos lo importante que es la imagen para una empresa o marca, es por ello que contamos con un equipo de profesionales altamente capacitados para darle una identidad gráfica adecuada y funcional a tus productos, con nuestro servicio de diseño gráfico podremos ofrecerte ayuda en el tema de: logos, publicidad digital, identidad corporativa, brochures y mucho más…
                    </p>

                </div>
                <div class="col-xs-12">
                    <a href="javascript:showLightbox();" id='DISEÑO GRÁFICO' class="click btn btn-primary btn-lg botonAzul">¡ ME INTERESA !</a>
                </div>
            </div>
        </div>
    </section>




    <section class="servicios">
        <div class="container-fluid">
            <div class="row">
                <div class="servicio1">
                    <div class="textServicio">
                        <h2>
                            ¡DISEÑAMOS TU IMAGEN!
                        </h2>
                        <h3>
                            Diseño gráfico a todo nivel y a tu alcance, nos adaptamos a tus ideas, le damos forma y color a tu visión.
                        </h3>
                        <div class="precio">
                            <h2 class="caviar">
                                Desde 430.000 COP (+IVA)
                            </h2>
                        </div>
                        <a href="javascript:showLightbox();" id='DISEÑO GRÁFICO' class="click btn btn-primary btn-lg botonWhite">¡ ME INTERESA !</a>

                    </div>
                </div>

            </div>
        </div>
    </section>

</div>

<section class="contact contact2">
    <div class="container">
        <div class="row">
            <?php contact(); ?>
        </div>
    </div>
</section>


<?php footer(); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Servicios').addClass('active');
    });
</script>
</body>
</html>
