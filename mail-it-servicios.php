<?php

/* Code by David McKeown - craftedbydavid.com */
/* Editable entries are bellow */
$click = $_POST["click"];


$send_to = "ludwig.angarita@lars.com.co";
$send_subject = "LARS - ".$click;

/*Be careful when editing below this line */

//Variables del formulario
$nombre = cleanupentries($_POST["nombre2"]);
$apellido = cleanupentries($_POST["apellido2"]);
$correo = cleanupentries($_POST["correo2"]);
$telefono = cleanupentries($_POST["telefono2"]);
$pais = cleanupentries($_POST["pais"]);
$empresa = cleanupentries($_POST["empresa"]);
$mensaje = cleanupentries($_POST["mensaje2"]);

function cleanupentries($entry) {
	$entry = trim($entry);
	$entry = stripslashes($entry);
	$entry = htmlspecialchars($entry);

	return $entry;
}

//Cuerpo del correo
$message = "Este correo fue enviado el " . date('m-d-Y') .
"\nNombre: " . $nombre . " " .$apellido .
"\n\nCorreo: " . $correo .
"\nTelefono: " . $telefono .
"\nPais: " . $pais .
"\nEmpresa: " . $empresa .
"\nMensaje: " . $mensaje .

"\n\n". $send_subject .= " - {$nombre}";

$headers = "From: " . $correo . "\r\n" .
    "Reply-To: " . $correo . "\r\n" .
    "X-Mailer: PHP/" . phpversion();

if (!$nombre) {
	echo "Completa todos los campos (Error de nombre!)";
	exit;
}else if (!$apellido){
	echo "Completa todos los campos (Error de apellido!)";
	exit;
}else if (!$correo){
	echo "Completa todos los campos (Error de correo!)";
	exit;
}else if (!$telefono){
	echo "Completa todos los campos (Error de teléfono!)";
	exit;
}else if (!$pais){
	echo "Completa todos los campos (Error de pais!)";
	exit;
}else if (!$empresa){
	echo "Completa todos los campos (Error de empresa!)";
	exit;
}else if (!$mensaje){
	echo "Completa todos los campos (Escribe un mensaje!)";
	exit;
}else{
	if (filter_var($correo, FILTER_VALIDATE_EMAIL)) {
		mail($send_to, $send_subject, $message, $headers);
	 	echo "<h1>¡LISTO!</h1>Esté muy atento, que en pocas horas nos estaremos comunicando con usted.";
	}else{
		echo "Ups! Parece que hubo un error, por favor inténtalo de nuevo!";
		exit;
	}
}

?>
