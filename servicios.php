<?php require_once("master.php"); cabecera(); ?>
<!--Start of Zendesk Chat Script-->
			<script type="text/javascript">
			window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
			d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
			$.src="//v2.zopim.com/?43bVlFst2la5AkPi1ywevcXcic3UTyPR";z.t=+new Date;$.
			type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
			</script>
			<!--End of Zendesk Chat Script-->



<section class="servicios">
    <div class="container-fluid">
        <div class="row">
            <div class="servicio1">
                <div class="textServicio">
                    <h2>
                    ¡EN 10 DÍAS!
                </h2>
                    <h3>
                    Creamos tu portal web fácil, rápido y adaptada a tu necesidad.
                </h3>
                    <div class="precio">
                        <h2 class="caviar">
                            Desde 430.000 COP (+IVA)
                        </h2>
                    </div>
                    <a href="sitioWeb.php#sitioWeb" class="btn btn-primary btn-lg botonWhite">¡ VER MAS !</a>

                </div>
            </div>


            <div class="servicio2">
                <div class="textServicio">
                    <h2>
                        ¿Cómo ganar dinero por internet?
                    </h2>
                    <h3>
                        Nuestros asesorías en E-commerce incluídas con la compra de tu tienda virtual con sistema de pago en línea.
                    </h3>
                    <div class="precio">
                        <h2 class="caviar">
                            Desde 800.000 COP (+IVA)
                        </h2>
                    </div>
                    <a href="sitioWeb.php#ecommerce" class="btn btn-primary btn-lg botonAzul">¡ VER MAS !</a>
                </div>
            </div>


            <div class="servicio3">
                <div class="textServicio">
                    <h2>
                    ¿Cómo vender en Facebook?
                </h2>
                    <h3>
                    Vender a través de Facebook ha revolucionado el mercado, Nuestros especialistas en Community Manager te mostrarán cómo.
                </h3>
                    <div class="precio">
                        <h2 class="caviar">
                            Desde 430.000 COP (+IVA)
                        </h2>
                    </div>
                    <a href="communityManager.php" class="btn btn-primary btn-lg botonWhite">¡ VER MAS !</a>

                </div>
            </div>


            <div class="servicio4">
                <div class="textServicio">
                    <h2 class="azul">
                    ¡DISEÑAMOS TU IMAGEN!
                </h2>
                    <h3>
                    Diseño gráfico a todo nivel y a tu alcance, nos adaptamos a tus ideas, le damos forma y color a tu visión.
                </h3>
                    <div class="precioAzul">
                        <h2 class="caviar">
                            Desde 430.000 COP (+IVA)
                        </h2>
                    </div>
                    <a href="disenoGrafico.php" class="btn btn-primary btn-lg botonAzul">¡ VER MAS !</a>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact">
    <div class="container">
        <div class="row">
            <?php contact(); ?>
        </div>
    </div>
</section>



<?php footer(); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Servicios').addClass('active');
    });
</script>
</body>
</html>
