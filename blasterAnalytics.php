<?php require_once("master.php"); cabecera(); ?>

<div class="blasterAnalitycs">

    <section class="sect4">
        <div class="container-fluid">
            <div class="row">
                <h1>Blaster Analitycs</h1>
                <div class="col-xs-12 text-center">
                    <div class="textMonitor">
                        <h2>¿Qué es?</h2>
                        <p>Blaster Analityc Es la herramienta desarrollada en Colombia de  análisis web que le permitirá rastrear las acciones de sus destinatarios una vez que ingresen a su página web y le otorgará las respuestas a sus preguntas, Usted puede determinar estadísticas básicas tales como el número de sus visitantes web, su origen geográfico, o el tiempo de permanencia, y muchas otras informaciones importantes.
                        </p>
                        <a class="btn btn-info btnMonitor" href="#blasterAnalitycs"><span class="glyphicon glyphicon-chevron-down"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="sect3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 id="blasterAnalitycs" class="azul">Fácil e Intuitivo</h1>
                    <p>
                        Nuestros diseñadores y desarrolladores encontaron la forma de hacer lo complejo, FÁCIL. Blaster Analytics, es increíblemente intuitiva tanto, que no necesitarás el uso de manuales ni tediosos cursos para usarla.
                    </p>
                    <h1 class="azul">¿ Tiene algun costo ?</h1>
                    <p>
                        Sí, Blaster Analytics tiene costo, y éste varía dependiendo de la modalidad de uso que escojas.
                    </p>
                    <h1 class="azul">On site</h1>
                    <p>
                        Se instala un servidor Activity Monitor en tus instalaciones y se configura de tal modo que tus administradores de IT puedan gestionarlo. (Costo inicial más alto)
                    </p>
                    <h1 class="azul">Como cloud service</h1>
                    <p>
                        Te brindaremos credenciales de acceso para tí y tus usuarios autorizados, para que accedas desde internet y realices los análisis que desees, adicionalmente tendrás un backup de LOG de Activity Monitor en nuestros potentes servidores. (Costo inicial más bajo).
                    </p>
                </div>
                <div class="col-xs-12">
                    <a href="javascript:showLightbox();" id='BLASTER ANALITYCS' class="click btn btn-primary btn-lg botonAzul">¡ ME INTERESA !</a>
                </div>
            </div>
        </div>
    </section>


    <section class="contact">
        <div class="container">
            <div class="row">
                <?php contact(); ?>
            </div>
        </div>
    </section>

</div>



<?php footer(); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Productos').addClass('active');
    });
</script>
</body>
</html>
