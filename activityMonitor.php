<?php require_once("master.php"); cabecera(); ?>

<div class="activityMonitor">

    <section class="sect4">
        <div class="container-fluid">
            <div class="row">
                <h1>Activity monitor ©</h1>
                <div class="col-xs-12 text-center">
                    <div class="textMonitor">
                        <h2>¿Tienes Realmente el control de lo que pasa en tu empresa?</h2>
                        <p>Pues ahora si lo puedes tener con Activity Monitor, con este software podrás ver en tiempo real lo que están haciendo tus empleados en sus estaciones de trabajo sin que ellos lo sepan, sabrás desde que página visitaron, cuantas
                            horas pasaron en Facebook, o incluso quién filtro <i>información confidencial</i> de su compañía.
                        </p>
                        <a class="btn btn-info btnMonitor" href="#activityMonitor"><span class="glyphicon glyphicon-chevron-down"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="sect3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 id="activityMonitor" class="azul">Activity monitor Enterprise ®</h1>
                    <p>
                        Beneficios como ver múltiples computadores en su red al mismo tiempo, generar informes, programar capturas de pantalla, registrar las pulsaciones del teclado en aplicaciones, correos electrónicos, páginas web y chats son lo que este software de última generación te ofrece, <a href="http://cdn.softactivity.com/cdn/activmon.exe">¡Descárgalo ahora!</a>
                    </p>
                    <h2>La productividad de tu Compañía es muy importante</h2>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <img class="img-responsive center-block img-thumbnail" src="img/remote.jpg" alt="">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 text-justify">
                    <h3>Supervisión OnLine y OffLine de sus empleados</h3>
                    <p>
                        Confíe, pero verifique. Descargue Gratis Activity Monitor hoy mismo y obtenga la mejor solución de supervisión de empleados para la red de su compañía.
                    </p>
                    <ul>
                        <li>Supervise a sus empleados sin que lo sepan</li>
                        <li>Vea las páginas web visitadas, aplicaciones usadas, chats de mensajería</li> instantánea, correo y más</li>
                        <li>Haga capturas de pantalla y genere informes para tomar medidas</li>
                        <li>Fácil de usar</li>
                        <li>1 año de Actualizaciones Y Soporte GRATUITOS</li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <a href="javascript:showLightbox();" id='ACTIVITY MONITOR' class="click btn btn-primary btn-lg botonAzul">¡ DESCARGAR GRATIS !</a>
                    <a href="javascript:showLightbox2();" id='ACTIVITY MONITOR COMPRAR' class="click btn btn-success btn-lg botonVerde">¡ COMPRAR AHORA !</a>
                </div>
            </div>
        </div>
    </section>





    <section class="contact">
        <div class="container">
            <div class="row">
                <?php contact(); ?>
            </div>
        </div>
    </section>

</div>


<?php footer(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Productos').addClass('active');
    });
    function showLightbox2() {
        document.getElementById('over2').style.display='block';
        document.getElementById('fade2').style.display='block';
    }
    function hideLightbox2() {
        document.getElementById('over2').style.display='none';
        document.getElementById('fade2').style.display='none';
    }
</script>
</body>

</html>
