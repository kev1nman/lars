<?php require_once("master.php"); cabecera(); ?>

<div class="communityManager">

    <section class="sect3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="azul">Community Manager Services</h1>
                    <div class="col-md-12 plan1">
                        <div class="col-md-3">
                            <h2 class="circuloPlan">PLAN 1</h2>

                        </div>
                        <div class="col-md-9 text-left">
                            <p>
                                Este paquete está diseñado exclusivamente para el uso de una sola red social (Twitter, Facebook o Instagram) con el fin de impulsar y promover su empresa.
                            </p>
                            <h2 class="azul">Características:</h2>
                            <h3>Creación o gestión de la cuenta escogida:</h3>
                            <ul>
                                <li><b>Twitter:</b> hasta 3 tweets diarios más interacciones (rt´s y respuestas*)</li>
                                <li><b>Facebook:</b> hasta 2 actualizaciones interdiarias.</li>
                                <li><b>Instagram:</b> hasta 2 foto diarias.</li>
                            </ul>
                            <h3>Desarrollo de la campaña campaña publicitaria:</h3>
                            <p>Análisis de su empresa para determinar lo que se va a comunicar en las redes sociales.</p>

                        </div>

                    </div>
                    <div class="col-md-12 plan2">
                        <div class="col-md-3">
                            <h2 class="circuloPlan">PLAN 2</h2>

                        </div>
                        <div class="col-md-9 text-left">
                            <p>
                                Este paquete incluye la contratación de dos redes sociales (Twitter, Facebook y/o Instagram) con el fin de impulsar y promover su empresa.
                            </p>
                            <h2 class="azul">Características:</h2>
                            <h3>Creación o gestión de la cuenta escogida:</h3>
                            <ul>
                                <li><b>Twitter:</b> hasta 3 tweets diarios más interacciones (rt´s y respuestas*)</li>
                                <li><b>Facebook:</b> hasta 2 actualizaciones interdiarias.</li>
                                <li><b>Instagram:</b> hasta 2 foto diarias.</li>
                                <li><b>Concurso mensual a través de las redes sociales.</b> </li>
                            </ul>
                            <h3>Desarrollo de la campaña campaña publicitaria:</h3>
                            <p>Análisis de su empresa para determinar lo que se va a comunicar en las redes sociales.</p>

                        </div>

                    </div>
                    <div class="col-md-12 plan1">
                        <div class="col-md-3">
                            <h2 class="circuloPlan">PLAN 3</h2>

                        </div>
                        <div class="col-md-9 text-left">
                            <p>
                                Aplica para empresas grandes que posean mucho material gráfico que publicitar: Este paquete incluye todas las redes sociales (Twitter, Facebook e Instagram) con el fin de impulsar y promover su empresa.

                            </p>
                            <h2 class="azul">Características:</h2>
                            <h3>Creación o gestión de la cuenta escogida:</h3>
                            <ul>
                                <li><b>Creación o gestión de la cuenta Twitter:</b> hasta 4 tweets diarios más iteracciones (rt´s y respuestas*) </li>
                                <li><b>Creación o gestión de la cuenta Facebook:</b> hasta 3 actualizaciones interdiarias. </li>
                                <li><b>Creación o gestión de la cuenta Instagram:</b> hasta 3 fotos diarias, incluyendo publicidad a otras páginas lo que incrementa los seguidores. </li>
                            </ul>
                            <h3>Desarrollo de la campaña campaña publicitaria:</h3>
                            <p>Análisis de su empresa para determinar lo que se va a comunicar en las redes sociales.</p>
                            <h3>Concurso mensual a través de las redes sociales. </h3>
                        </div>

                    </div>
                </div>
                <div class="col-xs-12">
                    <a href="javascript:showLightbox();" id='COMMUNITY MANAGER' class="click btn btn-primary btn-lg botonAzul">¡ SOLICITAR COTIZACIÓN !</a>
                </div>
            </div>
        </div>
    </section>


    <section class="contact">
        <div class="container">
            <div class="row">
                <?php contact(); ?>
            </div>
        </div>
    </section>

</div>



<?php footer(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Servicios').addClass('active');
    });
</script>
</body>

</html>
