<?php require_once("master.php"); cabecera(); ?>
<!--Start of Zendesk Chat Script-->
			<script type="text/javascript">
			window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
			d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
			$.src="//v2.zopim.com/?43bVlFst2la5AkPi1ywevcXcic3UTyPR";z.t=+new Date;$.
			type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
			</script>
			<!--End of Zendesk Chat Script-->
<div class="sitioWeb">
    <section class="sect3">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 id="sitioWeb">Página Web</h1>
                    <p>
                        Diseñamos páginas web adaptados a las necesidades de cada negocio y sus usuarios. Para ello nos esforzamos en conocer a nuestro cliente, involucrarnos con sus ideasy sus prioridades se convierten en las nuestras, podemos llevar a cabo desde un pequeño Website de contenidos e información básica de contacto hasta portales web robustos y con altos estándares de seguridad.
                    </p>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="cuadro">
                            <img src="img/icon/1.png" alt="">
                            <h3>Regístrate y se parte de nuestro equipo:</h3>
                            <p>Para nosotros es importante conocerte y que seas parte de nuestro equipo y de esta forma poder trabajar junto a ti y hacer realidad tus ideas. </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="cuadro">
                            <img src="img/icon/2.png" alt="">
                            <h3>Bienvenido a LARS:</h3>
                            <p>Una vez te hayas registrado como cliente, nuestro equipo se pondrá en contacto contigo, para darte la bienvenida a LARS y nos cuentes un poco de ti y levantar los requerimientos de tu proyecto.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="cuadro">
                            <img src="img/icon/3.png" alt="">
                            <h3>Háblanos de tu idea:</h3>
                            <p>El orden habla de nuestra ética como profesionales, es por ello que podrás contarnos de tu idea a detalle por medio de un formulario donde podremos esquematizar tu proyecto y darte un servicio de la mejor calidad.</p>
                        </div>
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="cuadro">
                            <img src="img/icon/4.png" alt="">
                            <h3>Que Comience la Magia</h3>
                            <p>Una vez definido y conciliado los requisitos del proyecto nuestro equipo de diseño gráfico, trabajará junto a ti, para crear propuestas que se adapten a tus ideas. Nuestros profesionales en desarrollo pondrán en marcha la construcción de tu proyecto, con los altos estándares que nos identifican.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="cuadro">
                            <img src="img/icon/5.png" alt="">
                            <h3>TU éxito Nuestro éxito</h3>
                            <p>Esto no termina con tu website, te ofrecemos soporte post venta y asesoría constante, estaremos allí para asesorarte y guiarte por el camino correcto para que tu proyecto digital alcance el éxito que esperas, desde asesoría en el modelado del negocio hasta el marketing digital, EL ÉXITO DE TU PROYECTO ES NUESTRO ÉXITO.</p>
                        </div>
                    </div>

                </div>
                <div class="col-md-12">
                    <a href="javascript:showLightbox();" id='PAGINA WEB' class="click btn btn-primary btn-lg botonAzul">¡ ME INTERESA !</a>
                </div>
            </div>
        </div>
    </section>

    <section class="ecommerce">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="monitorEcommerce">
                        <h1 id="ecommerce" class="marginTop">E-Commerce</h1>
                        <img src="img/monitor.png" class="img-responsive block-center" alt="">
                        <p class="textEcommerce">
                            Es momento de vender, en menos de 10 días puedes tener tu E-commerce adaptado a las necesidades de tu negocio,de la mano de un equipo de desarrollo que entiende a la perfección las exigencias del mercado a nivel de imagen y plataformas
                        </p>

                        <a href="javascript:showLightbox();" id='E-COMMERCE' class="click btn btn-primary btn-lg botonAzul">¡ ME INTERESA !</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="aplicacionesWeb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 id="aplicacionesWeb">Aplicaciones Web</h1>
                    <p class="text-center anchoP">Nuestros desarrollos cumplen con las normas internacionales ISO - SPICE (Software Process Improvement Capability Determination) CMM (Capability Maturity Model). y W3C</p>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <img src="img/responsive.jpg" class="img-responsive block-center img-thumbnail" alt="">
                    </div>
                    <div class="col-md-6">
                        <p class="text-justify">Todas las aplicaciónes que desarrollamos, las hacemos pensando en las tendencias del mercado, es decir ADAPTABILIDAD y ESCALABILIDAD. Tanto TÚ, como nosotros, queremos que tu empresa esté a la vanguardia y con tecnología de punta, por eso, las aplicaciones que desarrollemos para ti, cuentan con la garantía de las normas internacionales ISO.</p>
                        <div class="col-md-12 iconText noPadding">
                            <div class="col-xs-4 noPadding">
                                <img src="img/icon/4.png" alt="">
                            </div>
                            <div class="col-xs-8 text-left noPadding">
                                <h3>RESPONSIVE DESIGN</h3>
                                <p>Adaptable a cualquier dispositivo</p>
                            </div>
                        </div>
                        <div class="col-md-12 iconText noPadding">
                            <div class="col-xs-4 noPadding">
                                <img src="img/icon/7.png" alt="">
                            </div>
                            <div class="col-xs-8 text-left noPadding">
                                <h3>VELOCIDAD DE CARGA</h3>
                                <p>Las cosas lentas aburren</p>
                            </div>
                        </div>
                        <div class="col-md-12 iconText noPadding">
                            <div class="col-xs-4 noPadding">
                                <img src="img/icon/6.png" alt="">
                            </div>
                            <div class="col-xs-8 text-left noPadding">
                                <h3>BONITO Y FÁCIL DE USAR</h3>
                                <p>Ante todo tu imagen</p>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:showLightbox();" id='APLICACIONES WEB' class="click btn btn-primary btn-lg botonAzul">¡ ME INTERESA !</a>
                </div>
            </div>
        </div>
    </section>

    <section class="aplicacionesMoviles">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-xs-12 col-sm-6 col-md-6 porqNosotros">
                        <h1 id="aplicacionesMoviles">Aplicaciones Móviles</h1>
                        <div class="caja">
                            <p>
                                Si quieres que tu negocio éste al alcance de todos y en cualquier momento, nuestro equipo de LARS Software Company desarrollará para ti la mejor aplicación móvil innovadora y funcional acorde a sus requerimientos
                            </p>
                        </div>
                        <div class="flecha"></div>
                        <a href="javascript:showLightbox();" id='APLICACIONES MÓVILES' class="click btn btn-primary btn-lg botonAzul">¡ ME INTERESA !</a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <img class="img-responsive center-block" src="img/iphone.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contact contact3">
        <div class="container">
            <div class="row">
                <?php contact(); ?>
            </div>
        </div>
    </section>

</div>




<?php footer(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#Servicios').addClass('active');
    });
</script>
</body>

</html>
